<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>word编辑窗体</title>
<style type="text/css">
#DsoFramerOfficeCtrl span {
	color: red;
}
body {
	height: 100%;
}
</style>
</head>

<body>
	<button type="button" class="btn btn-primary" onclick="saveDoc()"
		id="save">
		保存
	</button>
	<button type="button" class="btn" onclick="window.close();" id="close">
		关闭
	</button>
	<div class="" id="editdocBox" style="height: 680px;">
	<%@include file="DsoFramer.jsp"%>
	</div>
	</div>
	<script type="text/javascript">
		var currentEditUser = "小韶";
		var readOnlyModel = "${readOnlyModel}";
		/*提交在线控件中内容*/
		function saveDoc() {
			var uploadUrl = "<%=basePath%>/office/upload";
			var DsoFramerOfficeCtrl = document.getElementById('DsoFramerOfficeCtrl');
			if (DsoFramerOfficeCtrl	&& typeof (DsoFramerOfficeCtrl.CreateNew) != "undefined") {
				try {
					DsoFramerOfficeCtrl.HttpInit();
					DsoFramerOfficeCtrl.HttpAddPostCurrFile("file","Dso.doc");
					DsoFramerOfficeCtrl.HttpPost(uploadUrl);
					if (confirm("操作完成是否关闭此窗体？"))
						window.close();
				} catch (e) {
					alert("提交在线控件中内容到服务器错误!\n" + e.message);
				}

			}
		}
		/*打开模板到在线控件中*/
		function toEditDoc() {
			var openUrl = "<%=basePath%>/office/editdoc";
			var DsoFramerOfficeCtrl = document.getElementById('DsoFramerOfficeCtrl');
			if (DsoFramerOfficeCtrl) {
				try {
					DsoFramerOfficeCtrl.Open(openUrl, true,"Word.Document", "", "");
					if (readOnlyModel != 'true') {
						DsoFramerOfficeCtrl.SetCurrUserName(currentEditUser); //设置修改的用户
						DsoFramerOfficeCtrl.SetTrackRevisions(1); //进入留痕状态 
					}
				}catch (e) {
					alert("从服务器打开文档到在线编辑控件错误!\n可能无法在线修改表格。\n" + e.message
							+ "\n请尝试在IE中设本系统地址为信任站点重试。");
				} 
			} else {
				alert("你的浏览器未安装在线编辑控件!\n可能无法在线修改表格，可使用上传附件的方式。");
			}
		}
		/*调用这个控件时,不能直接调用这个toEditDoc()方法,调用方法时控制一下时间,不然会报灾难性错误的!
		估计是控件还没加载,就调用方法,所以保存错的!
		*/
		setTimeout("toEditDoc()", 200);
		
	</script>
</body>
</html>