package com.dso.web.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class DsoAction extends ActionSupport {
	private static Logger log = Logger.getLogger(DsoAction.class);
	private static final long serialVersionUID = 3479489709236477762L;
	private File file;
	public String excute() throws Exception {
		return "";
	}
	/**
	 * 上传方法,及保存文档的方法
	 * @return
	 * @throws Exception
	 */
	public String upload() throws Exception {
		log.info("执行上传方法");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddhhmmssSS");
		String tarurl=ServletActionContext.getServletContext().getRealPath("/doc");
		String newFileName=null;
		newFileName=sdf.format(new Date());
		newFileName+="DSO.doc";
        File savefile = new File(tarurl, newFileName);
        if (!savefile.getParentFile().exists())        
        	savefile.getParentFile().mkdirs();//创建目录  
        FileUtils.copyFile(file, savefile);  //将临时文件复制指定文件夹;     
		return "success";
	}

	public String dowload() throws Exception {
		log.info("执行下载方法");
		return "";
	}
	/**
	 * 获取文档内容,稍微修改即使下载方法
	 * @throws Exception
	 */
	public void getEditContent() throws Exception {
		log.info("获取doc的内容");
		HttpServletResponse response = ServletActionContext.getResponse();
		InputStream is = null;
		OutputStream os = null;
		try {
			String url=ServletActionContext.getServletContext()
					.getRealPath("doc/DSO.doc");
			is = new FileInputStream(url);
			os = response.getOutputStream();
			byte adjuBuff[] = new byte[0x32000];
			int readLen = 0;
			while ((readLen = is.read(adjuBuff)) > -1) {
				os.write(adjuBuff, 0, readLen);
			}
		} finally {
			try {
				if (is != null)
					is.close();
				os.flush();
				if (os != null)
					os.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
}
